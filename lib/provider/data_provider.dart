import 'package:flutter/material.dart';

class DataProvider with ChangeNotifier {
  int _counter = 0;

  int get counter {
    return _counter;
  }

  increase() {
    _counter++;
    notifyListeners();
  }

  decrease() {
    _counter--;
    notifyListeners();
  }
}
