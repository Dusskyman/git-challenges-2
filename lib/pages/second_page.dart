import 'package:flutter/material.dart';
import 'package:lecture_practice_5/provider/data_provider.dart';
import 'package:provider/provider.dart';

class SecondPage extends StatelessWidget {
  showSnaks(BuildContext context, int counter) {
    Scaffold.of(context).hideCurrentSnackBar();
    Scaffold.of(context).showSnackBar(
        SnackBar(content: Text('Number was changed to: $counter')));
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<DataProvider>(context, listen: false);
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(),
      body: Builder(
        builder: (context) {
          return Center(
            child: Container(
              color: Colors.lightGreen[100],
              height: 100,
              width: 100,
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      data.increase();
                      showSnaks(context, data.counter);
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.remove),
                    onPressed: () {
                      data.decrease();
                      showSnaks(context, data.counter);
                    },
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
